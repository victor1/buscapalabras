#!/usr/bin/env pythoh3

'''
Busca una palabra en una lista de palabras
'''
import sys

import sortwords

def search_word(word, words_list):
    try:
        word in words_list
        for x in range(0, len(words_list)):
            if word == words_list[x]:
                position = x
                return position
    except:
        raise ValueError
def main():
    words_list = sys.argv[2:]
    word = sys.argv[1]
    ordered_list = sorted(words_list)
    sortwords.show(ordered_list)
    position = search_word(word,words_list)
    print(position)


if __name__ == '__main__':
    main()
